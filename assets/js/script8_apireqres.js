let endpoint = 'https://reqres.in/api/users'
let content = document.getElementById('card-container')

// POST METHOD
let Mahasiswa = {data: [
    {
        nama: 'Ega',
        job: 'mahasiswa'
    },
    {
        nama: 'Syahrul',
        job: 'mahasiswa'
    },
    {
        nama: 'Ramadhanto',
        job: 'mahasiswa'
    },
    {
        nama: 'Ega Syahrul Ramadhanto',
        job: 'mahasiswa'
    },
]};

let fetchOptions = {
    method: "POST",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify(Mahasiswa)
};

// fetch(endpoint, fetchOptions)
// .then(response => response.json())
// .then(data => console.log(data))
// .catch(error => console.error(error));


fetch(endpoint, fetchOptions).then((res)=> res.json()).then((response)=> console.log(response)).catch(error => console.error(error));

// GET METHOD
const ambil = fetch(endpoint)
.then((res) => res.json())
.then((response) => {
    // response.forEach(element => {
    //     console.log(element);
    // });
    // console.log(response.data[3]);
    response.data.forEach(element => {
        // console.log(element.data);
        content.innerHTML += `
        <div class="card">
            <img src="${element.avatar}" alt="Image" class="card-image">
            <h3 class="card-title">${element.first_name} ${element.last_name}</h3>
            <span class="card-body">${element.email}</span>
        </div>
        `;
    });
})