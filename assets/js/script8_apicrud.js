let endpointpostdata = 'https://crudcrud.com/api/51275dc4e92149009467d84d9adc22df/user'
let endpointgetdata = 'https://crudcrud.com/api/51275dc4e92149009467d84d9adc22df/user/65fac28c1492af03e8f0f034'
let content = document.getElementById('card-container')

// POST METHOD
let Mahasiswa = {mahasiswa: [
    {
        nama: 'Ega',
        usia: 22
    },
    {
        nama: 'Syahrul',
        usia: 22
    },
    {
        nama: 'Ramadhanto',
        usia: 22
    },
    {
        nama: 'Ega Syahrul Ramadhanto',
        usia: 22
    },
]};

let fetchOptions = {
    method: "POST",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify(Mahasiswa)
};

fetch(endpointpostdata, fetchOptions)
.then(response => response.json())
.then(data => console.log(data))
.catch(error => console.error(error));


fetch(endpointpostdata, fetchOptions).then((res)=> res.json()).then((response)=> console.log(response)).catch(error => console.error(error));


// GET METHOD
const ambil = fetch(endpointgetdata)
.then((res) => res.json())
.then((response) => {
    // response.forEach(element => {
    //     console.log(element);
    // });
    console.log(response);
    response.mahasiswa.forEach(element => {
        console.log(element);
        content.innerHTML += `
        <div class="card">
            <img src="../assets/img/ega.png" alt="Image" class="card-image">
            <h3 class="card-title">${element.nama}</h3>
            <span class="card-body">${element.usia}</span>
        </div>
        `;
    });
})